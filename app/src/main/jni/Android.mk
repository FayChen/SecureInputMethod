LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

OPENCV_CAMERA_MODULES:=on
OPENCV_INSTALL_MODULES:=on
OPENCV_LIB_TYPE:=SHARED

include E:\\sdk\\OpenCV-2.4.9-android-sdk\\sdk\\native\\jni\\OpenCV.mk

LOCAL_MODULE    := HandGestureApp
LOCAL_SRC_FILES := jni_process.cpp preProcessAndDetection.cpp
LOCAL_LDLIBS +=  -llog -ldl -latomic


include $(BUILD_SHARED_LIBRARY)